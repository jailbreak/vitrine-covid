Le Hackathon COVID se clôturera ce samedi 24 avril par la restitution des 15 projets participants. 

Cette restitution sera l'occasion pour les équipes d'exposer au grand public les solutions créées, imaginées, et pour certaines prototypées, chacune visant à répondre à un des [quatre défis](https://hackathon-covid.fr/#defis) de ce hackathon. 
