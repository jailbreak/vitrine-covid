Ils contribuent aussi à l'événement :
* Le Ministère des Solidarités et de la Santé
* La Haute Autorité de Santé
* L'Agence Régionale de Santé Île de France
* La Délégation ministérielle au Numérique en Santé
* La Direction Générale de la Santé
* Le Centre de Crise Sanitaire
* L'AP-HP
* Covidom
* L'URPS Médecins IdF
* Météo-France
