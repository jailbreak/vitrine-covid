Retrouvez ici le programme du Hackathon

## Vendredi 22 avril 2021 : 

* **9h-9h30** : ouverture de la salle et accueil des participants 
* **9h30-9h45** : mot d'accueil et introduction à l'outil Gather.town
* **9h45-10h** : discours inaugural 
* **10h** : pitch des porteurs de projets 
* **11h** : constitution des équipes et début des micros ouverts
* **18h** : fin des micros ouverts


## Samedi 23 avril 2021 : 

* **9h** : début du travail des équipes
* **9h30** : début des micros ouverts
* **17h-19h** : discours de clôture et restitution des projets
