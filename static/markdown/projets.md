Vous êtes [inscrit au hackathon](https://www.eventbrite.fr/e/billets-hackathon-covid-19-151037662715) ? Vous souhaitez porter un projet qui répond à l'un des quatre défis ? Rendez-vous [sur le forum](https://forum.hackathon-covid.fr/) pour présenter votre idée, votre équipe et vos compétences. 

Pour déposer son projet, suivez la procédure suivante : 

1. Inscrivez-vous sur le [forum](https://forum.hackathon-covid.fr/) et confirmez votre adresse mail.
2. Rendez-vous à l'[adresse suivante](https://forum.hackathon-covid.fr/c/les-projets/6) ou cliquez sur la catégorie "Projets".
2. Choisissez le défi auquel vous souhaitez répondre.
3. Cliquez sur "créer un sujet".
4. Complétez les informations dont vous disposez déjà. Ce même post sera à compléter au fur et à mesure de l'événement et de l'avancée de votre projet.
