Nous, citoyens, entreprises, chercheurs, administrations publiques, voulons unir nos forces pour contribuer à mettre fin à la pandémie. Les différentes vagues de l’épidémie ont vu la mobilisation d’un écosystème d’acteurs publics et privés très actifs qui a abouti à la création de nouvelles ressources et des services plébiscités par le grand public comme Covid Tracker, Brisons la Chaîne et plus récemment Covidliste. 

**Initié par des acteurs de la société civile et organisé avec le soutien de la Direction Interministérielle de la Transformation Publique dans le cadre du dispositif “gouvernement ouvert”, un marathon de 48h de conception et développement est organisé les 23 et 24 avril 2021. Son objectif : renforcer les outils et solutions mises à disposition des Français face à la pandémie.** 

Nous voulons mettre autour de la table les institutions en charge de la lutte contre la pandémie ainsi que les acteurs de la société civile engagés ou qui souhaitent s’engager dans la lutte contre la pandémie. Pendant deux jours, dans un espace en ligne retrouvant l’effervescence du travail collaboratif en présentiel, les participants pourront initier de nouveaux projets, contribuer à leur développement, s’inspirer avec des retours d’expérience et des sessions de formation et rencontrer des acteurs mobilisés dans la lutte contre la pandémie.  Ce hackathon sera l’occasion d’amplifier le mouvement d’ouverture des données et du code source déjà engagé par plusieurs administrations Nous sommes convaincus que l’ouverture et la collaboration répondent de manière plus efficace et rapide aux enjeux sans cesse renouvelés de la crise. 


**Qui peut participer ?**


Ce hackathon est ouvert à tous ceux qui, détenteurs d’une idée, d’une compétence, d’un savoir-faire ou d’une curiosité pour les données liées à la Covid-19, souhaitent œuvrer pour créer des solutions communes de lutte contre la pandémie. Tous les profils sont les bienvenus, en particulier : 
* les spécialistes de la donnée (data analyst, data scientist, etc.) avides de donnée à croiser, combiner et visualiser,
* les développeurs dont les compétences en programmation permettront de donner vie au projet de leur équipe,
* les designers et autres concepteurs d’usage, capables de penser l’intégration de solutions dans le quotidien des utilisateurs, 
* les chercheurs capables de mobiliser un savoir scientifique au profit d’un projet,
* les citoyens inspirés qui souhaitent faire part de leurs besoins de solutions sur les 4 défis du Hackathon.


Vous êtes créatif et motivé, **disponible sur les deux jours du hackathon** et prêt à monter ou intégrer une équipe pour développer un projet ? [Inscrivez-vous](https://www.eventbrite.fr/e/billets-hackathon-covid-19-151037662715) et choisissez l’un des 4 défis identifiés en lien avec les partenaires publics et le monde de la recherche.


