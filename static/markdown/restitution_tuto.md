Cette restitution se déroulera sous la forme d'un salon. 

Chaque équipe occupera un stand portant le numéro de leur projet (voir la liste des numéros [ici](https://hackathon-covid.fr/projets)). Ce stand aura été construit au préalable par l'équipe elle-même, qui laissera transparaitre par son design et son architecture toute la spécificité de leur solution. 

![](https://github.com/ArthurSrz/images/blob/main/Screenshot%202021-04-24%20at%2011.32.59.png)

