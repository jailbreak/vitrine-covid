import { ArrayModel, ObjectModel } from "objectmodel"

import Navbar from "./navbar"
import Page from "./page"

const ConfigModel = new ObjectModel({
	version: 3,
	title: [String],
	navbar: [Navbar],
	pages: ArrayModel(Page),
	filterTag: [String],
	markdownBaseUrl: [String],
}).as("Config")

export default class Config extends ConfigModel {
	findPageBySlug(slug) {
		return this.pages.find((page) => page.slug === slug)
	}
}
