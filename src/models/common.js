// From https://objectmodel.js.org/docs/examples/common.js

import { BasicModel } from "objectmodel"

export const PositiveNumber = BasicModel(Number)
	.assert(function isPositive(n) {
		return n >= 0
	})
	.as("PositiveNumber")
export const PositiveInteger = PositiveNumber.extend().assert(Number.isInteger).as("PositiveInteger")
