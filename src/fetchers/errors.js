import OError from "@overleaf/o-error"

export class ConfigError extends OError {
	constructor(options) {
		super("Config error", options)
	}
}

export class FetchError extends OError {
	constructor(options) {
		super("Fetch error", options)
	}
}
